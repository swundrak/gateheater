//
// Simple Webserver class that serves html pages from the SD-Card
//
// Written by Stefan Wundrak, 2013

#include <Ethernet.h>


typedef void (*ResolveVariablePtr)(char *varname, Client &client);

class WebServer
{
  public:
    WebServer();
    
    void begin(ResolveVariablePtr fptr);
    void Loop();

  private:
    void SendPage(Client &client, char *page);
    void ParseRequest(Client &client, char *page, unsigned int len);
    unsigned int ReadLine(Client &client, char *buf, unsigned int len);
    
private:

  // Initialize the Ethernet server library
  // with the IP address and port you want to use
  // (port 80 is default for HTTP):
  EthernetServer server;
  
  ResolveVariablePtr resolve;

};
