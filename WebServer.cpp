
#include "WebServer.h"

#include <Ethernet.h>

WebServer::WebServer():server(80)
{
  
}
    
void WebServer::begin(ResolveVariablePtr fptr)
{
  server.begin();
  Serial.print("Server is at ");
  Serial.println(Ethernet.localIP());

   resolve = fptr;
}
    
void WebServer::Loop()
{
  
  
  
  //-- Ethernet Listen for incoming clients
  EthernetClient client = server.available();
  if (client) 
  {
    Serial.println("New Request");
    char page[100];
    ParseRequest( client, page, 1024 );
    
    SendPage(client, page);
    
    // give the web browser time to receive the data
    delay(1);
 
    // close the connection:
    client.stop();
    Serial.println("client disonnected");
  }
}


void WebServer::SendPage(Client &client, char *page)
{

      // send a standard http response header
      client.println("HTTP/1.1 200 OK");
      client.println("Content-Type: text/html");
      client.println("Connnection: close");
      client.println();
      client.println("<!DOCTYPE HTML>");

      // Read file from flash and replace variables $VAR_NAME$ with value of call-back function!
      client.println("<html>");
      client.println("<br>Room 0: "); resolve("TR0", client);
      client.println("<br>Room 1: "); resolve("TR1", client);
      client.println("<br>Room 2: "); resolve("TR2", client);
      client.println("<br>Room 3: "); resolve("TR3", client);
      client.println("<br>Room 4: "); resolve("TR4", client);
      client.println("<br>Room 5: "); resolve("TR5", client);
      client.println("<br>Room 6: "); resolve("TR6", client);
      client.println("<br>Room 7: "); resolve("TR7", client);
     
      client.println("</html>");   
  
}

void WebServer::ParseRequest(Client &client, char *page, unsigned int len)
{
    
       
      unsigned int lineLength = ReadLine(client, page, len);
    
      
      // GET Request?
      if (lineLength>4 && page[0]=='G' && page[1]=='E' && page[2]=='T')
      {
          
          //-- Copy Page part to start
          unsigned int pos = 0;
          while( pos+4<lineLength && page[pos+4]!=' ' && pos < len-1) 
          {
            page[pos] = page[pos+4];
            pos++;
          }
          page[pos++]=0;
  
          Serial.print("GET Request: ");  
          Serial.println(page);
         
    }
  
    // Read and ignore rest of request
    while (client.connected() && client.available())
    { 
        unsigned int lineLength = ReadLine(client, page, len);
    }
      
      
   
}

unsigned int WebServer::ReadLine(Client &client, char *buf, unsigned int len)
{   
  unsigned int pos = 0;
  
  while (client.connected()) 
  {
      if (client.available()) 
      {
         char c = client.read();
         Serial.write(c);
       
         if (c =='\n') return pos;
         buf[pos] = c;
         pos++;
         buf[pos] = 0;
         if (pos >= len-1) return pos;
      }
   }
}

