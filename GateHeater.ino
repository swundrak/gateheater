


/*
 
 
 Circuit:
 * Ethernet shield attached to pins 10, 11, 12, 13
 * FS20_WUE to 8,9 (RxD,TxD)
 
 */

#include <SPI.h>
#include <Ethernet.h>
#include <SoftwareSerial.h>      // From IDE 1.01
#include "Clock.h"
#include "WebServer.h"



// Enter a MAC address and IP address for your controller below.
// The IP address will be dependent on your local network:
byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xF1, 0xE0 };
IPAddress ip(192,168,178, 101);

WebServer server;


SoftwareSerial mySerial(8, 9);       // RxD, TxD
Clock clock;

// Commands for the ELV Weather Receiver FS20WUE. There are some typos in the documentation of the
// receiver. This are the right commands:
char* cmdStat  = "\x02\x01\xF0";        // Returns status of weather data receiver
char* cmdWDOn  = "\x02\x02\xF2\x01";    // Receive weather data and transmit immediately
char* cmdFSOn  = "\x02\x02\xF1\x01";    // Receive FS20 data and transmit immediately
char* cmdText  = "\x02\x02\xFB\x01";    // All output in text format
char* cmdBin   = "\x02\x02\xFB\x00";    // All output in binary      // "\x02\x02\xFB\x01";    // All output in text format
char* cmdReset = "\x02\x01\xF6";        // All output in binary      // "\x02\x02\xFB\x01";    // All output in text format

struct RoomData
{
  int temp;
  int moist;  
};

RoomData room[8];

void PrintHex8(uint8_t *data, uint8_t length) // prints 8-bit data in hex with leading zeroes
{
        Serial.print("0x"); 
        for (int i=0; i<length; i++) { 
          if (data[i]<0x10) {Serial.print("0");} 
          Serial.print(data[i],HEX); 
          Serial.print(" "); 
        }
}

void PrintHex8(uint8_t data)
{
        Serial.print("0x"); 
        if (data < 0x10) {Serial.print("0");} 
        Serial.print(data,HEX); 
        Serial.print(" ");         
}


void PrintVariable(char *varname, Client &client)
{
  // This function is called by our WebServer class to
  // Resolve dynamic variables
  
  if (varname[0]=='T' && varname[1]=='R')
  { 
      unsigned char i = (int)varname[2] - 48;  // ASCII 0 = 48
      if (i < 8) client.print(room[i].temp);  
  }
   
}

void setup() 
{
 // Open serial communications and wait for port to open:
  Serial.begin(9600);


   // start the Ethernet connection and the server:
   Ethernet.begin(mac, ip);
   
   server.begin(PrintVariable);
 
  
  // Setup the clock. In Germany we are 1 hours ahead of GMT 
  clock.Setup();
  clock.SetTimezoneOffset(1, 0); 
  
  // Set the data rate for the SoftwareSerial port
  mySerial.begin(4800);
  delay(100);
  //mySerial.print(cmdBin);
  //delay(10);
  mySerial.print(cmdFSOn);
  delay(10);
  mySerial.print(cmdWDOn);
  delay(10);
  //mySerial.print(cmdStat);
  //delay(10);
}



void doWireless()
{
  //-- 
  if (mySerial.available())
  {  
      
    // Read from FS20WUE
    //Serial.write(mySerial.read());   // Write to PC serial
    //unsigned char cc = mySerial.read();
    //if (cc==0x02)  Serial.println("");  
    //PrintHex8(cc);
    //return;
    
    
    unsigned char c = 0;
    
    c = mySerial.read();delay(10);
    Serial.println("");
    //PrintHex8(c);
    
    if (c==0x02)
    {
      
      c = mySerial.read(); delay(10);//PrintHex8(c);       
      if (c!=0x0C) return;   // only Weatherstation data
      
      c = mySerial.read();delay(10); //PrintHex8(c);
      if (c!=0xA2) return;   // Answer ID
      
      c = mySerial.read();delay(10); //PrintHex8(c);
      if (c!=0x01) return;   // Sensor Type
      
     
      unsigned char adr = mySerial.read(); delay(10); //PrintHex8(adr);
      
      int temp;
      ((unsigned char*)(&temp))[1] = mySerial.read();delay(10);
      ((unsigned char*)(&temp))[0] = mySerial.read();delay(10);
      
      unsigned int moist;
      ((unsigned char*)(&moist))[1] = mySerial.read();delay(10);
      ((unsigned char*)(&moist))[0] = mySerial.read();delay(10);
      
      mySerial.read();delay(10);mySerial.read();delay(10);mySerial.read();delay(10);mySerial.read();delay(10); mySerial.read(); // Ignore Wind & Rain
      
      clock.WriteDateTime(&Serial);
      Serial.print(" Id: ");PrintHex8(adr); Serial.print(" ");
      Serial.print("Temp: ");Serial.print(temp); Serial.print(" ");      
      Serial.print("Moist: ");Serial.print(moist);
      
      room[adr].temp = temp;
      room[adr].moist = moist;
      
     
       
    }
    

  }
  
  
  //if (Serial.available())            // Read from PC serial
  //  mySerial.write(Serial.read());   // Write to FS20WUE

}

void loop() 
{ 
    clock.Maintain();
    
    doWireless();
    
    server.Loop();
  
}
